/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import { graphql, useStaticQuery } from "gatsby"
import PropTypes from "prop-types"
import * as React from "react"
import { Helmet } from "react-helmet"

function Seo({ description, title, children, imageurl }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `
  )

  const metaDescription = description || site.siteMetadata.description
  const defaultTitle = site.siteMetadata?.title
  const url = site.siteMetadata?.url

  return (
    <Helmet>
      <title>
        {defaultTitle
          ? `${title} | ${defaultTitle}`
          : title || "Tymczasem pod lasem"}
      </title>
      <meta name="description" content={metaDescription} />
      <meta property="og:title" content={title} />
      <meta property="og:url" content={url || "https://tymczasempodlasem.pl"} />
      <meta property="og:description" content={metaDescription} />
      <meta property="og:type" content="website" />
      <meta property="og:updated_time" content="123465789" />
      <meta
        property="og:image"
        content="https://tymczasempodlasem.pl/logoglowne.jpg"
      />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:creator" content={site.siteMetadata?.author || ``} />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={metaDescription} />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta
        name="keywords"
        content="koty, tymczasempodlasem, dom tymczasowy, adopcja kotów, koty do adopcji, bezdomne koty, kociaki, kotka, kocur, Hajnówka, Toruń, schronisko dla kotóœ, pomoc kotom, koty do adopcji, koty do adopcji Toruń, koty do adopcji Hajnówka, kotki do adopcji, małe koty do adopcji"
      />
      {children}
    </Helmet>
  )
}

Seo.defaultProps = {
  description: ``,
}

Seo.propTypes = {
  description: PropTypes.string,
  title: PropTypes.string.isRequired,
}

export default Seo
