import React, { useMemo, useRef, useState } from "react"
import styled from "styled-components"
import closeIcon from "../../images/icons/close.svg"
import searchIcon from "../../images/icons/search.svg"
import NotFoundImage from "../../images/notfound-image.png"
import { removePolishChars } from "../../shared/helpers"
import CatCard from "../CatCard"

export const SearchButton = styled.button`
  padding: 12px;
  width: 48px;
  height: 48px;
`

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;

  input {
    background: #fdfdfd;
    box-shadow: 1px 1px 4px rgba(67, 66, 66, 0.25);
    border-radius: 50px;
    padding: 0.5rem 1rem;
    font-size: 18px;
    font-family: "Be Vietnam";
    border: none;
  }
`

export const SearchResults = styled.div`
  position: fixed;
  top: 80px;
  left: 50%;
  transform: translateX(-50%);
  width: 100%;
  height: 100%;
  background-color: #fff;
  max-width: 1920px;
  padding: 20px 24px;
  height: 100vh;
  overflow-y: scroll;
  overflow-x: hidden;
  z-index: 100;
  display: ${props => (props.visible ? "block" : "none")};

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    top: 190px;
  }
`

const List = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  flex-wrap: wrap;
  gap: 25px 5px;
  margin: 40px 0;
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 4rem 0;
  gap: 20px;
  text-align: center;
`

const Image = styled.img`
  max-width: 200px;
`

const Input = styled.input`
  display: ${props => (props.visible ? "block" : "none")};
`

const SearchForm = ({ data }) => {
  const [isInputVisible, setIsInputVisible] = useState(false)
  const [value, setValue] = useState("")
  const inputRef = useRef(null)

  const filteredData = useMemo(() => {
    return data.allStrapiCat.edges.filter(
      ({ node }) =>
        node.imie
          .toLowerCase()
          .replace(" ", "")
          .includes(value.toLowerCase().replace(" ", "")) &&
        node.zdjecia &&
        node.zdjecia.length
    )
  }, [value])

  const onSearchClick = () => {
    if (isInputVisible) {
      setValue("")
      setIsInputVisible(false)
    } else {
      setIsInputVisible(true)
      setTimeout(() => {
        inputRef.current.focus()
      }, [100])
    }
  }

  return (
    <Container>
      <Input
        visible={isInputVisible}
        type="text"
        ref={inputRef}
        alt=""
        value={value}
        onChange={e => setValue(e.target.value)}
        placeholder="Wpisz imię kota"
      />
      <SearchButton onClick={onSearchClick}>
        <img
          src={isInputVisible ? closeIcon : searchIcon}
          alt="Zamknij wyszukiwanie"
        />
      </SearchButton>
      <SearchResults visible={value.length > 0}>
        <p style={{ paddingTop: 20, paddingBottom: 30 }}>
          Wyniki wyszukiwania dla "{value}"
        </p>

        {filteredData.length ? (
          <List>
            {filteredData.map(({ node }) => {
              let href
              switch (node.statusKota) {
                case "niegotowy":
                  href = `/nasi-podopieczni/jeszcze-niegotowe/${removePolishChars(
                    node.imie.toLowerCase().replace(" ", "")
                  )}`
                  break
                case "do adopcji":
                  href = `/nasi-podopieczni/koty-do-adopcji/${removePolishChars(
                    node.imie.toLowerCase().replace(" ", "")
                  )}`
                  break
                case "adoptowany":
                  href = `/nasi-podopieczni/w-swoich-domkach/${removePolishChars(
                    node.imie.toLowerCase().replace(" ", "")
                  )}`
                  break
                default:
                  href = ""
                  break
              }
              return (
                <CatCard
                  img={node.zdjecia[0].url}
                  name={node.imie.replace(" ", "")}
                  href={node.statusKota === "adoptowany" ? null : href}
                />
              )
            })}
          </List>
        ) : (
          <Wrapper>
            <Image src={NotFoundImage} alt="Nie znaleziono" />
            <span>Nie odnaleźliśmy dopasowań dla Twojego zapytania :(</span>
          </Wrapper>
        )}
      </SearchResults>
    </Container>
  )
}

export default SearchForm
