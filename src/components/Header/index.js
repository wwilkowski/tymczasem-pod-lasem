import { graphql, Link, StaticQuery } from "gatsby"
import * as React from "react"
import closeIcon from "../../images/icons/close.svg"
import hamburgerIcon from "../../images/icons/hamburger.svg"

import logo from "../../images/logo.svg"
import SearchForm from "../SearchForm"
import {
  Buttons,
  Logo,
  MenuItem,
  MenuLabel,
  MenuLink,
  MenuList,
  MenuToggler,
  Row,
  StyledHeader,
  Submenu,
  SubmenuLink,
} from "./style"

const items = [
  {
    name: "O nas",
    to: "/",
  },
  {
    name: "Nasi podopieczni",
    to: "/nasi-podopieczni",
    submenu: [
      {
        name: "Koty do adopcji",
        to: "/nasi-podopieczni/koty-do-adopcji",
      },

      {
        name: "Jeszcze niegotowe",
        to: "/nasi-podopieczni/jeszcze-niegotowe",
      },
      {
        name: "W swoich domkach",
        to: "/nasi-podopieczni/w-swoich-domkach",
      },
    ],
  },
  {
    name: "Przed adopcją",
    to: "/przed-adopcja",
    submenu: [
      {
        name: "Warunki adopcji",
        to: "/przed-adopcja/warunki-adopcji",
      },
      {
        name: "Jak przygotować się do adopcji kota?",
        to: "/przed-adopcja/przygotowanie",
      },
    ],
  },
  {
    name: "Zostań Tymczasem",
    to: "/zostan-tymczasem",
  },
  {
    name: "Wesprzyj nas",
    to: "/wesprzyj-nas",
    submenu: [
      // {
      //   name: "Czego potrzebujemy",
      //   to: "/wesprzyj-nas/czego-potrzebujemy",
      // },
      {
        name: "Zakup karmę dla kotów",
        to: "/wesprzyj-nas/kup-karme",
      },
      {
        name: "Zobacz aktualne zrzutki",
        to: "/wesprzyj-nas/aktualne-zrzutki",
      },
      // {
      //   name: "Zostań naszym patronem",
      //   to: "/wesprzyj-nas/zostan-patronem",
      // },
    ],
  },
  {
    name: "Kontakt",
    to: "/kontakt",
  },
]

const Header = ({ location }) => {
  const [menuVisible, setMenuVisible] = React.useState(false)

  return (
    <StyledHeader>
      <Row>
        <Link to="/">
          <Logo src={logo} alt="Logo Tymczasem pod lasem" />
        </Link>
        <Buttons>
          <StaticQuery
            query={graphql`
              {
                allStrapiCat {
                  edges {
                    node {
                      id
                      imie
                      kastracja
                      plec
                      stosunekDoKotow
                      statusKota
                      wiek
                      specjalnePotrzeby
                      zdjecia {
                        url
                      }
                    }
                  }
                }
              }
            `}
            render={data => <SearchForm data={data} />}
          />

          <MenuToggler onClick={() => setMenuVisible(prev => !prev)}>
            <img src={menuVisible ? closeIcon : hamburgerIcon} alt="Menu" />
          </MenuToggler>
        </Buttons>
      </Row>
      <nav>
        <MenuList visible={menuVisible}>
          <MenuLabel>Menu</MenuLabel>
          {items.map(item => (
            <MenuItem>
              <MenuLink to={item.to}>
                {item.name}

                {item.submenu && (
                  <Submenu>
                    {item.submenu.map(subitem => (
                      <li>
                        <SubmenuLink to={subitem.to}>
                          {subitem.name}
                        </SubmenuLink>
                      </li>
                    ))}
                  </Submenu>
                )}
              </MenuLink>
            </MenuItem>
          ))}
        </MenuList>
      </nav>
    </StyledHeader>
  )
}

export default Header
