import { Link } from "gatsby"
import styled from "styled-components"

export const StyledHeader = styled.header`
  background-color: ${({ theme }) => theme.colors.background};
  padding: 20px 24px;
  display: flex;
  flex-direction: column;
  position: sticky;
  top: 0;
  width: 100vw;
  z-index: 100;
  max-width: 1920px;
  margin: 0 auto;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    position: sticky;
    top: 0;
    padding: 20px 100px;
  }
`

export const Logo = styled.img`
  width: 200px;
  height: 40px;
  margin: 0;

  @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
    width: 213px;
    height: 40px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    width: 256px;
    height: 48px;
  }
`

export const Buttons = styled.div`
  display: flex;
  align-items: center;
`

export const MenuToggler = styled.button`
  padding: 12px;
  width: 48px;
  height: 48px;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: none;
  }
`

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

export const MenuList = styled.ul`
  display: ${({ visible }) => (visible ? "flex" : "none")};
  height: 100vh;
  flex-direction: column;
  align-items: flex-end;
  padding: 12px;
  text-align: right;

  @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
    align-items: center;
    text-align: center;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: flex;
    white-space: nowrap;
    height: auto;
    flex-direction: row;
    justify-content: flex-end;
    margin-top: 2rem;
    gap: 2.75rem;
  }

  li {
    margin: 5px 0;
  }
`

export const MenuLabel = styled.p`
  font-size: 1.75rem;
  font-weight: ${({ theme }) => theme.fontWeight.medium};
  position: relative;
  padding: 1rem 0;
  margin-bottom: 0.8125rem;

  &::after {
    content: "";
    position: absolute;
    bottom: 0;
    right: 0;
    background-color: ${({ theme }) => theme.colors.accent};
    height: 3px;
    width: 150px;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      left: 50%;
      transform: translateX(-50%);
      width: 300px;
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: none;
  }
`

export const MenuItem = styled.li``

export const Submenu = styled.ul`
  list-style-type: none;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin: 0;

  li {
    margin: 5px 0;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: none;
    position: absolute;
    top: 100%;
    left: -10px;
    background-color: ${({ theme }) => theme.colors.background};
    z-index: 100;
    padding: 0.625rem;
    width: 200px;
    text-align: left;
  }
`

export const MenuLink = styled(Link)`
  color: ${({ theme }) => theme.colors.primaryText};
  text-decoration: none;
  font-size: 1.1875rem;
  position: relative;

  &:focus,
  &:hover {
    font-weight: ${({ theme }) => theme.fontWeight.medium};

    ${Submenu} {
      display: block;
    }
  }

  &::after {
    content: "";
    position: absolute;
    left: -20px;
    top: 11px;
    transform: translateY(50%);
    width: 5px;
    height: 5px;
    border-radius: 50px;
    background-color: ${({ theme }) => theme.colors.primaryText};

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      display: none;
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    font-size: 1.3125rem;
  }
`

export const SubmenuLink = styled(Link)`
  text-align: right;
  color: ${({ theme }) => theme.colors.primaryText};
  text-decoration: none;
  font-size: 0.9375rem;
  white-space: normal;

  &:focus,
  &:hover {
    font-weight: ${({ theme }) => theme.fontWeight.medium};
  }
`
