import React from "react"
import { components } from "react-select"
import styled, { css } from "styled-components"
import ArrowDown from "../../images/arrow-down.png"

const StyledSelect = styled.select`
  font-size: 16px;
  line-height: 23px;
  min-width: 250px;
  font-family: "Be Vietnam";
  padding: 10px;
  -moz-appearance: none; /* Firefox */
  -webkit-appearance: none; /* Safari and Chrome */
  appearance: none;
  background-color: #fff;
  padding-right: 30px;
  cursor: pointer;

  option {
    padding-left: 20px;
  }

  @media (min-width: 480px) {
    font-size: 18px;
  }

  @media (min-width: 1024px) {
    font-size: 21px;
    line-height: 32px;
  }
`

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 10px;
  margin-bottom: 24px;
  position: relative;

  &::after {
    content: "";
    position: absolute;
    top: 50%;
    width: 20px;
    height: 20px;
    background-image: url(${ArrowDown});
    background-size: cover;
    right: 12px;
    transform: translateY(-50%);
    pointer-events: none;
  }
`
const Input = styled.input`
  appearance: none;
  background-color: transparent;
  margin: 0;
  font: inherit;
  color: currentColor;
  width: 20px;
  height: 20px;
  border: 2px solid red;
  border-radius: 50%;

  &:checked {
    background: transparent;
  }
`

const InputWrapper = styled.div`
  height: 100%;
  ${({ checked }) =>
    checked &&
    css`
      position: relative;
      &::before {
        content: "";
        position: absolute;
        border-radius: 50%;
        border: 2px solid green;
        background-color: ${({ theme }) => theme.colors.accent};
        top: 48%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 13px;
        height: 13px;
      }
    `}
`

const Option = props => {
  return (
    <div>
      <components.Option {...props}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            flexWrap: "nowrap",
            gap: 10,
          }}
        >
          <InputWrapper checked={props.isSelected}>
            <Input
              type="radio"
              checked={props.isSelected}
              onChange={() => null}
            />{" "}
          </InputWrapper>
          <p style={{ whiteSpace: "nowrap" }}>{props.label}</p>
        </div>
      </components.Option>
    </div>
  )
}
export default Option
