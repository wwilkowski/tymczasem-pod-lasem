import { Link } from "gatsby"
import { GatsbyImage } from "gatsby-plugin-image"
import React from "react"
import Slider from "react-slick"
import "slick-carousel/slick/slick-theme.css"
import "slick-carousel/slick/slick.css"
import styled from "styled-components"
import Modal from "../../shared/Modal"

const Container = styled.div`
  width: 100%;
  position: relative;
  margin-bottom: 40px;
  padding: 0;
  max-width: 1400px;
  margin-left: auto;
  margin-right: auto;

  .slick-list {
    margin: 0 -20px;
  }
  .slick-slide > div {
    padding: 0 20px;
  }

  @media (min-width: 1024px) {
    padding: 0 6rem;
    margin-top: 30px;
    margin-bottom: 150px;
  }
`

const LeftArrow = styled.button`
  position: absolute;
  left: -10px;
  top: 50%;
  transform: translate(0, -50%);

  background-color: #212422;
  border-radius: 50%;
  width: 50px;
  height: 50px;
  z-index: 100;

  @media (min-width: 1024px) {
    left: 0;
  }

  &::before {
    content: "<";
    font-size: 30px;
    font-weight: 600;
    position: absolute;
    left: 0;
    top: -3px;
    color: #fff;
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`

const RightArrow = styled.button`
  position: absolute;
  right: -10px;
  top: 50%;
  transform: translate(0, -50%);

  background-color: #212422;
  border-radius: 50%;
  width: 50px;
  height: 50px;

  @media (min-width: 1024px) {
    right: 0;
  }

  &::before {
    content: ">";
    font-size: 30px;
    font-weight: 600;
    position: absolute;
    left: 0;
    top: -3px;
    color: #fff;
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`

const StyledGatsbyImage = styled(GatsbyImage)`
  height: 300px;
  width: 100%;
  @media (min-width: 1150px) {
    height: 400px;
  }
`

const StaticImages = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;

  @media (min-width: 600px) {
    flex-direction: row;
  }
`

const settings = {
  dots: false,
  slidesToShow: 3,
  slidesToScroll: 1,

  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      },
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
}

const Carousel = ({ images }) => {
  const [showModal, setShowModal] = React.useState(false)
  const [modalImage, setModalImage] = React.useState(null)

  const slider = React.useRef(null)

  return (
    <Container>
      <Modal
        showModal={showModal}
        setShowModal={setShowModal}
        image={modalImage}
      />
      {images.length >= 3 ? (
        <>
          <LeftArrow onClick={() => slider?.current?.slickPrev()} />
          <Slider {...settings} className="overflow-hidden" ref={slider}>
            {images.map(image => (
              <Link
                to="#"
                onClick={e => {
                  e.preventDefault()
                  setShowModal(true)
                  setModalImage(image)
                }}
              >
                <div className="tplimage">
                  <img src={`https://admin.tymczasempodlasem.pl${image}`} />
                </div>
              </Link>
            ))}
          </Slider>
          <RightArrow onClick={() => slider?.current?.slickNext()} />
        </>
      ) : images.length > 1 ? (
        <StaticImages>
          {images.map(image => (
            <Link
              to="#"
              onClick={e => {
                e.preventDefault()
                setShowModal(true)
                setModalImage(image)
              }}
            >
              <div>
                <img src={`https://admin.tymczasempodlasem.pl${image}`} />
              </div>
            </Link>
          ))}
        </StaticImages>
      ) : null}
    </Container>
  )
}

export default Carousel
