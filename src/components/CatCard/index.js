import { Link } from "gatsby"
import { GatsbyImage } from "gatsby-plugin-image"
import React from "react"
import styled from "styled-components"
import Modal from "../../shared/Modal"
import { GlobalLink } from "../../shared/styles"

const Card = styled.div`
  max-width: 400px;
  width: 100%;
  padding-bottom: 2rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
`

const Image = styled.img`
  width: 100%;
  max-width: 400px;
`

const Content = styled.div`
  background: #fdfdfd;
  box-shadow: 1px 1px 8px rgba(0, 0, 0, 0.1);
  padding: 12px 24px;
  height: -webkit-fill-available;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

const Important = styled.span`
  font-weight: 600;
  display: inline-block;
  margin: 10px 0;
`

const StyledGatsbyImage = styled(GatsbyImage)`
  width: 100%;
  aspect-ratio: 1 / 1;

  @media (min-width: 480px) {
    height: 400px;
    width: 400px;
  }
`

const CatCard = ({ img, name, descriptionData, href }) => {
  const [showModal, setShowModal] = React.useState(false)
  const [modalImage, setModalImage] = React.useState(null)

  return (
    <Card>
      <Modal
        showModal={showModal}
        setShowModal={setShowModal}
        image={modalImage}
      />
      <Link
        to={href || "#"}
        onClick={e => {
          if (!href) {
            e.preventDefault()
            setShowModal(true)
            setModalImage(img)
          }
        }}
      >
        <div className="tplimage">
          <img src={`https://admin.tymczasempodlasem.pl${img}`} />
        </div>
      </Link>
      <Content>
        <div>
          <h3>{name}</h3>
          {descriptionData
            ? Object.keys(descriptionData)
                .filter(k => descriptionData[k])
                .map(k => {
                  return (
                    <div>
                      <p>
                        <Important>{k}:</Important> {descriptionData[k]}
                      </p>
                    </div>
                  )
                })
            : null}
        </div>
        {href ? (
          <GlobalLink to={href} style={{ margin: "40px auto" }}>
            Zobacz więcej
          </GlobalLink>
        ) : null}
      </Content>
    </Card>
  )
}

export default CatCard
