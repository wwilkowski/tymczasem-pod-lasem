/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import { graphql, useStaticQuery } from "gatsby"
import "gatsby-plugin-breadcrumb/gatsby-plugin-breadcrumb.css"
import * as React from "react"
import styled, { createGlobalStyle, ThemeProvider } from "styled-components"
import { theme } from "../shared/theme"
import Footer from "./Footer"
import Header from "./Header"
import "./layout.css"

const GlobalStyle = createGlobalStyle`
  *, *::before, *::after {
    box-sizing: border-box;
  }



  button {
    border: 0;
    background: none;
    cursor: pointer;
  }
`

const Container = styled.div`
  padding-top: 5rem;
  padding: 0;
  margin: 0 auto;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding-top: 0;
  }
`

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  React.useEffect(() => {
    let element = document.getElementById("portal")
    // if element is not found with wrapperId,
    // create and append to body
    if (!element) {
      const wrapperElement = document.createElement("div")
      wrapperElement.setAttribute("id", "portal")
      document.body.appendChild(wrapperElement)
    }
  }, [])

  return (
    <>
      <ThemeProvider theme={theme}>
        <GlobalStyle theme="purple" />
        <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
        <Container>
          <main>{children}</main>
          <Footer />
        </Container>
      </ThemeProvider>
    </>
  )
}

export default Layout
