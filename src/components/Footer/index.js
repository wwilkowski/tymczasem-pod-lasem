import { Link } from "gatsby"
import React from "react"
import styled from "styled-components"
import FacebookIcon from "../../images/icons/facebook.svg"
import InstagramIcon from "../../images/icons/instagram.png"
import MessengerIcon from "../../images/icons/messenger.png"

import logo from "../../images/logo.svg"

const StyledFooter = styled.footer`
  border-top: 2px solid ${({ theme }) => theme.colors.accent};
  padding: 24px;
`

const Logo = styled.img`
  width: 128px;
  height: 24px;
  display: block;
  margin: 0 auto;

  @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
    width: 213px;
    height: 40px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    width: 256px;
    height: 48px;
    margin: 0;
  }
`

export const ColumnTitle = styled.p`
  color: ${({ theme }) => theme.colors.accent};
  margin: 10px auto;
  text-align: center;
  text-transform: uppercase;
  font-weight: 600;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    margin: 10px 0;
    text-align: left;
  }
`

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;

  a {
    color: ${({ theme }) => theme.colors.primaryText};
  }
`

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  padding-bottom: 20px;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    flex-direction: row;
    align-items: flex-start;
    justify-content: space-around;
  }
`

export const ContactColumn = styled.div`
  display: none;
  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: flex;
    flex-direction: column;
  }
`

export const List = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 10px;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    align-items: flex-start;
  }

  a {
    color: ${({ theme }) => theme.colors.primaryText};
  }
`

const TextWithIcon = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 10px;

  img {
    width: 25px;
  }
`

const FacebookLink = styled(Link)`
  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding-top: 1rem;
  }
`

const Footer = () => {
  return (
    <StyledFooter>
      <Wrapper>
        <Logo src={logo} alt="Logo Tymczasem pod lasem" />
        <ContactColumn>
          <ColumnTitle>Przydatne linki</ColumnTitle>
          <List>
            <li>
              <Link to="/nasi-podopieczni/koty-do-adopcji">
                Koty do adopcji
              </Link>
            </li>
            <li>
              <Link to="/przed-adopcja/warunki-adopcji">Warunki adopcji</Link>
            </li>
            <li>
              <Link href="/umowa-adopcyjna.pdf" target="_blank">
                Umowa adopcyjna
              </Link>
            </li>
            <li>
              <Link to="/wesprzyj-nas/czego-potrzebujemy">
                Czego potrzebujemy?
              </Link>
            </li>
          </List>
        </ContactColumn>
        <Column>
          <ColumnTitle>Kontakt</ColumnTitle>
          <List>
            <li>
              <Link href="https://m.me/tymczasempodlasem" target="_blank">
                <TextWithIcon>
                  <img src={MessengerIcon} alt="Messenger" />
                  Messenger
                </TextWithIcon>
              </Link>
            </li>
            <li>
              <Link
                href="https://www.instagram.com/kotyztymczasempodlasem/"
                target="_blank"
              >
                <TextWithIcon>
                  <img src={InstagramIcon} alt="Instagram" />
                  Instagram
                </TextWithIcon>
              </Link>
            </li>
          </List>
        </Column>
        <Column>
          <FacebookLink
            href="https://www.facebook.com/tymczasempodlasem"
            target="_blank"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <TextWithIcon>
              <img src={FacebookIcon} alt="Facebook" />
              Polub nas na Facebooku!
            </TextWithIcon>
          </FacebookLink>
        </Column>
      </Wrapper>
      <Column>
        2023 &copy; Projekt:{" "}
        <a
          href="https://www.facebook.com/jaros.kinga"
          style={{ color: "#BB2205", display: "contents" }}
        >
          Kinga Jaros
        </a>
        , Wykonanie:{" "}
        <a
          href="https://www.facebook.com/wwilkowski"
          style={{ color: "#BB2205", display: "contents" }}
        >
          Wojciech Wilkowski
        </a>
      </Column>
    </StyledFooter>
  )
}

export default Footer
