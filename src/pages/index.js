import { graphql } from "gatsby"
import * as React from "react"
import styled from "styled-components"
import Carousel from "../components/Carousel"
import Layout from "../components/layout"
import Seo from "../components/seo"
import Action1 from "../images/homescreen-actions-1.svg"
import Action2 from "../images/homescreen-actions-2.svg"
import Action3 from "../images/homescreen-actions-3.svg"
import Image1Desktop from "../images/homescreen-image-1-desktop.png"
import Image1Mobile from "../images/homescreen-image-1-mobile.png"
import Image1Tablet from "../images/homescreen-image-1-tablet.png"
import Image2Desktop from "../images/homescreen-image-2-desktop.png"
import Image2Mobile from "../images/homescreen-image-2-mobile.png"
import Image2Tablet from "../images/homescreen-image-2-tablet.png"
import Image3Desktop from "../images/homescreen-image-3-desktop.png"
import Image3Mobile from "../images/homescreen-image-3-mobile.png"
import Image3Tablet from "../images/homescreen-image-3-tablet.png"
import { Flex, GlobalLink, Space } from "../shared/styles"

const Content = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    gap: 27px;
    padding: 36px;
    padding-bottom: 0;

    a {
      margin-top: 20px;
    }
  }
`

const FirstSection = styled(Flex)`
  margin-bottom: 50px;
  gap: 20px;
  padding: 0 24px;

  @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
    margin-bottom: 100px;
  }

  @media (min-width: 1300px) {
    gap: 14px;
    padding: 3rem 0 0 24px;
    &::before {
      content: "";
      position: absolute;
      bottom: 10%;
      left: 0;
      width: 100%;
      height: 96px;
      background-color: ${({ theme }) => theme.colors.accent};
      z-index: -1;
    }
  }
`

const SecondSection = styled(Flex)`
  gap: 20px;
  margin: 60px auto 60px auto;
  padding: 0 24px;

  img {
    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      object-position: center;
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    gap: 14px;
    margin: 0 auto 120px auto;
  }

  picture {
    position: relative;

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      &::after {
        content: "";
        position: absolute;
        right: -30px;
        bottom: -30px;
        height: 317px;
        width: 96px;
        background-color: ${({ theme }) => theme.colors.accent};
        z-index: -1;
      }
    }
  }
`

const ThirdSection = styled(Flex)`
  margin-bottom: 30px;
  gap: 20px;
  padding: 0 24px;

  img {
    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      object-position: center;
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    gap: 14px;
    margin: 0 auto 120px auto;
    padding: 0 0 0 24px;
  }

  picture {
    position: relative;

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      &::after {
        content: "";
        position: absolute;
        left: -30px;
        bottom: -30px;
        height: 317px;
        width: 96px;
        background-color: ${({ theme }) => theme.colors.accent};
        z-index: -1;
      }
    }
  }
`

const ActionsList = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 60px;
  width: 50%;
  text-align: center;
  padding: 24px;
  margin: 0 auto;

  @media (min-width: 480px) {
    margin: 4rem auto;

    flex-direction: row;
    justify-content: center;
    width: 100%;
  }
`

const ActionsElement = styled.li`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 10px;
  padding: 10px;

  &:nth-child(2) {
    position: relative;

    &::before {
      content: "";
      height: 24px;
      width: 2px;
      background-color: ${({ theme }) => theme.colors.accent};
      position: absolute;
      bottom: 115%;
      left: 50%;
      transform: translateX(-50%);
    }

    &::after {
      content: "";
      height: 24px;
      width: 2px;
      background-color: ${({ theme }) => theme.colors.accent};
      position: absolute;
      top: 115%;
      left: 50%;
      transform: translateX(-50%);
    }

    @media (min-width: 480px) {
      &::before {
        content: "";
        width: 64px;
        height: 3px;
        background-color: ${({ theme }) => theme.colors.accent};
        position: absolute;
        left: -64px;
        top: 50%;
        transform: translate(0%, 50%);
      }

      &::after {
        content: "";
        width: 64px;
        height: 3px;
        background-color: ${({ theme }) => theme.colors.accent};
        position: absolute;
        left: 100%;
        top: 50%;
        transform: translate(0%, 50%);
      }
    }
  }

  img {
    width: 40px;
    height: 40px;

    @media (min-width: 480px) {
      width: 72px;
      height: 72px;
    }
  }

  p {
    max-width: 250px;
  }
`

const CarouselWrapper = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  gap: 30px;
`

const IndexPage = ({ data }) => {
  return (
    <Layout>
      <Seo title="Tymczasem pod lasem" />
      <FirstSection>
        <picture>
          <source
            style={{ width: "100%" }}
            media="(max-width: 480px)"
            srcset={Image1Mobile}
            alt=""
          />
          <source
            style={{ width: "100%" }}
            media="(max-width: 1024px)"
            srcset={Image1Tablet}
            alt=""
          />
          <img style={{ width: "100%" }} src={Image1Desktop} alt-="" />
        </picture>
        <Content style={{ maxWidth: 800 }}>
          <h1>Znajdź z nami przyjaciela na zawsze!</h1>
          <p>Podaruj dom jednemu z naszych podopiecznych</p>
          <GlobalLink to="/nasi-podopieczni/koty-do-adopcji">
            Zobacz koty do adopcji
          </GlobalLink>
        </Content>
      </FirstSection>
      <SecondSection reverse alignItems="flex-end">
        <picture>
          <source
            style={{ width: "100%" }}
            media="(max-width: 480px)"
            srcset={Image2Mobile}
            alt=""
          />
          <source
            style={{ width: "100%" }}
            media="(max-width: 1024px)"
            srcset={Image2Tablet}
            alt=""
          />
          <img style={{ width: "100%" }} src={Image2Desktop} alt-="" />
        </picture>
        <Content style={{ maxWidth: 700 }}>
          <h2>Kim jesteśmy?</h2>
          <p>
            We współpracy z kilkoma domami tymczasowymi pomagamy kotom z
            obszarów wiejskich z rejonów Hajnówki i Torunia. Działamy prywatnie,
            w miarę możliwości.
          </p>
          <p>
            W okolicy Hajnówki szanse na adopcje są nikłe dlatego szukamy domów
            w całej Polsce.
          </p>
          <GlobalLink to="/zostan-tymczasem">
            Zobacz jak zostać tymczasem
          </GlobalLink>
        </Content>
      </SecondSection>
      <ThirdSection alignItems="flex-end">
        <picture>
          <source
            style={{ width: "100%" }}
            media="(max-width: 480px)"
            srcset={Image3Mobile}
            alt=""
          />
          <source
            style={{ width: "100%" }}
            media="(max-width: 1024px)"
            srcset={Image3Tablet}
            alt=""
          />
          <img style={{ width: "100%" }} src={Image3Desktop} alt-="" />
        </picture>
        <Content style={{ maxWidth: 600 }}>
          <h2>Nasza misja</h2>
          <p>
            Dajemy kotom wolno żyjącym oraz potrzebującym pomocy szansę na
            kochający dom: zabieramy je z miejsca bytowania, poddajemy
            diagnostyce weterynaryjnej, leczeniu oraz zabiegom kastracji,
            organizujemy dla nich miejsca w domach tymczasowych, a także
            przeprowadzamy procedury adopcyjne.
          </p>
          <GlobalLink to="/nasi-podopieczni/koty-do-adopcji">
            Zobacz koty do adopcji
          </GlobalLink>
        </Content>
      </ThirdSection>
      <div>
        <ActionsList>
          <ActionsElement>
            <img src={Action1} alt="" />
            <p>Zabieramy z miejsca bytowania</p>
          </ActionsElement>
          <ActionsElement>
            <img src={Action2} alt="" />
            <p>Koordynujemy leczenie i kastracje</p>
          </ActionsElement>
          <ActionsElement>
            <img src={Action3} alt="" />
            <p>Szukamy kochającego domku</p>
          </ActionsElement>
        </ActionsList>
      </div>
      <Space>
        <CarouselWrapper>
          <h2>Kadry z akcji</h2>
          <Carousel
            images={data.allStrapiKadrZAkcji.nodes[0].zdjecia.map(
              photo => photo.url
            )}
          />
        </CarouselWrapper>
      </Space>
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query MyQuery {
    allStrapiKadrZAkcji {
      nodes {
        zdjecia {
          url
        }
      }
    }
  }
`
