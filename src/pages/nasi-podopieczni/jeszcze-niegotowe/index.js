import { graphql } from "gatsby"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React, { useEffect } from "react"
import styled from "styled-components"
import CatCard from "../../../components/CatCard"
import Layout from "../../../components/layout"
import Seo from "../../../components/seo"
import TitleIcon from "../../../images/jeszcze-niegotowe-title-icon.svg"
import { getAge, removePolishChars } from "../../../shared/helpers"

const Container = styled.div`
  padding: 0 24px;
  position: relative;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 0 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const List = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  flex-wrap: wrap;
  gap: 25px 5px;
  margin: 10px 0 40px 0;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    margin: 40px 0;
  }
`

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
  gap: 20px;
  width: 100%;

  h2 {
    position: relative;
    padding-right: 80px;
    width: 100%;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
      width: fit-content;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }
`

const Icon = styled.img`
  height: 56px;
  display: none;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: block;
  }
`

const JeszczeNiegotowePage = ({ pageContext, data }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  useEffect(() => {
    const interval = setInterval(() => {
      window.location.reload()
    }, 120000)

    return () => clearInterval(interval)
  }, [])

  return (
    <Layout>
      <Seo title="Jeszcze niegotowe" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <TitleWrapper>
          <h2>Jeszcze niegotowe</h2>
          <Icon src={TitleIcon} alt="" />
        </TitleWrapper>
        <div style={{ padding: "24px 0" }}>
          <p style={{ display: "inline" }}>
            W tym albumie prezentujemy koty, które przyjeliśmy pod swoje
            skrzydła niedawno i wymagają opieki weterynaryjnej.
          </p>
        </div>
        {data.allStrapiCat.totalCount === 1 && (
          <p style={{ marginBottom: 24 }}>
            {data.allStrapiCat.totalCount} kot będzie niebawem szukało swojego
            domu.
          </p>
        )}
        {data.allStrapiCat.totalCount > 1 &&
          data.allStrapiCat.totalCount <= 4 && (
            <p style={{ marginBottom: 24 }}>
              {data.allStrapiCat.totalCount} koty będzie niebawem szukało
              swojego domu.
            </p>
          )}
        {data.allStrapiCat.totalCount > 4 && (
          <p style={{ marginBottom: 24 }}>
            {data.allStrapiCat.totalCount} kotów będzie niebawem szukało swojego
            domu.
          </p>
        )}

        <List>
          {data.allStrapiCat.edges
            .filter(({ node }) => node.zdjecia && node.zdjecia.length > 0)
            .map(({ node }) => (
              <CatCard
                img={node.zdjecia[0].url}
                name={node.imie.replace(" ", "")}
                descriptionData={{
                  wiek: getAge(node.dataUrodzenia),
                  płeć: node.plec,
                  "stosunek do kotów": node.stosunekDoKotow,
                  kastracja: node.kastracja,
                  "specjalne potrzeby": node.specjalnePotrzeby,
                }}
                href={removePolishChars(
                  node.imie.toLowerCase().replace(" ", "")
                )}
              />
            ))}
        </List>
      </Container>
    </Layout>
  )
}

export default JeszczeNiegotowePage

export const query = graphql`
  {
    allStrapiCat(filter: { statusKota: { in: ["niegotowy"] } }) {
      totalCount
      edges {
        node {
          id
          imie
          kastracja
          opis {
            data {
              opis
            }
          }
          plec
          stosunekDoKotow
          statusKota
          dataUrodzenia
          specjalnePotrzeby
          zdjecia {
            url
          }
        }
      }
    }
  }
`
