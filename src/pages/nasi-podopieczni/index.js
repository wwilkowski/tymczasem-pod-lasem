import { Link } from "gatsby"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React from "react"
import styled from "styled-components"
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Image1 from "../../images/nasi-podopieczni-image-1.png"
import Image2 from "../../images/nasi-podopieczni-image-2.png"
import Image3 from "../../images/nasi-podopieczni-image-3.png"

const List = styled.ul`
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 10px;
  align-items: center;
  width: 100%;
  margin: 30px 0 24px 0;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    margin: 60px 0 24px 0;
    flex-direction: row;
  }
`

const ListElement = styled.li`
  width: 100%;
  overflow: hidden;
  max-width: 500px;

  a {
    display: flex;
    aspect-ratio: 1.5 / 1;
    color: ${({ theme }) => theme.colors.white};
    position: relative;
    justify-content: center;
    align-items: center;
    text-align: center;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      aspect-ratio: unset;
      height: 150px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      aspect-ratio: 1.5 / 1;
      height: auto;
    }
  }

  h4 {
    white-space: nowrap;
  }

  img {
    position: absolute;
    left: 0;
    top: 0;
    z-index: -1;
    width: 100%;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      top: 50%;
      transform: translateY(-50%);
    }

    &::after {
      content: "";
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: -1;
    }
  }
`

const Container = styled.div`
  padding: 0 24px;
  position: relative;

  h2 {
    position: relative;
    padding-right: 80px;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 50px 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const NasiPodopieczniPage = ({ pageContext, location }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  return (
    <Layout>
      <Seo title="Nasi podopieczni" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <h2>Nasi podopieczni</h2>
        <List>
          <ListElement>
            <Link to="koty-do-adopcji">
              <img src={Image1} alt="" />
              <h4>Koty do adopcji</h4>
            </Link>
          </ListElement>
          <ListElement>
            <Link to="jeszcze-niegotowe">
              <img src={Image2} alt="" />
              <h4>Jeszcze niegotowe</h4>
            </Link>
          </ListElement>
          <ListElement>
            <Link to="w-swoich-domkach">
              <img src={Image3} alt="" />
              <h4>W swoich domkach</h4>
            </Link>
          </ListElement>
        </List>
      </Container>
    </Layout>
  )
}

export default NasiPodopieczniPage
