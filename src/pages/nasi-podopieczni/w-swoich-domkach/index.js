import { graphql } from "gatsby"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React, { useEffect, useState } from "react"
import ReactSelect from "react-select"
import styled from "styled-components"
import CatCard from "../../../components/CatCard"
import Option from "../../../components/Option"
import Layout from "../../../components/layout"
import Seo from "../../../components/seo"
import TitleIcon from "../../../images/w-swoich-domkach-title-icon.svg"
import { getAge, removePolishChars } from "../../../shared/helpers"

const Container = styled.div`
  padding: 0 24px;
  position: relative;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 0 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
  gap: 20px;
  width: 100%;

  h2 {
    position: relative;
    padding-right: 80px;
    width: 100%;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
      width: fit-content;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }
`

const Icon = styled.img`
  height: 56px;
  display: none;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    display: block;
  }
`

const List = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  flex-wrap: wrap;
  gap: 25px 5px;
  margin: 10px 0 40px 0;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    margin: 40px 0;
  }
`

const Row = styled.div`
  display: flex;
  flex-direction: column;

  @media (min-width: 770px) {
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }
`

const SelectWrapper = styled.div`
  display: flex;

  flex-direction: row;
  align-items: center;
  gap: 10px;
  margin-bottom: 24px;
`

const WSwoichDomkachPage = ({ pageContext, data }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  const [selected, setSelected] = useState({
    value: "najnowsze",
    label: "od najnowszych",
  })

  const [sorted, setSorted] = useState([])

  const handleChange = s => {
    setSelected(s)

    switch (s.value) {
      case "az":
        setSorted(
          data.allStrapiCat.edges.sort((a, b) =>
            a.node.imie > b.node.imie ? 1 : -1
          )
        )
        break
      case "za":
        setSorted(
          data.allStrapiCat.edges.sort((a, b) =>
            a.node.imie < b.node.imie ? 1 : -1
          )
        )
        break
      case "najmlodsze":
        setSorted(
          data.allStrapiCat.edges.sort((a, b) =>
            a.node.dataUrodzenia < b.node.dataUrodzenia ? 1 : -1
          )
        )
        break
      case "najstarsze":
        setSorted(
          data.allStrapiCat.edges.sort((a, b) =>
            a.node.dataUrodzenia > b.node.dataUrodzenia ? 1 : -1
          )
        )
        break
      case "najnowsze":
        setSorted(
          data.allStrapiCat.edges.sort((a, b) =>
            new Date(a.node.createdAt) < new Date(b.node.createdAt) ? 1 : -1
          )
        )
        break

      default:
        setSorted(data.allStrapiCat.edges)
        break
    }
  }

  useEffect(() => {
    setSorted(
      data.allStrapiCat.edges.sort((a, b) =>
        new Date(a.node.createdAt) < new Date(b.node.createdAt) ? 1 : -1
      )
    )
  }, [data])

  useEffect(() => {
    const interval = setInterval(() => {
      window.location.reload()
    }, 120000)

    return () => clearInterval(interval)
  }, [])

  return (
    <Layout>
      <Seo title="W swoich domkach" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <TitleWrapper>
          <h2>W swoich domkach</h2>
          <Icon src={TitleIcon} alt="" />
        </TitleWrapper>
        <div style={{ padding: "24px 0" }}>
          <p style={{ display: "inline" }}>
            Kotki, które odnalazły swój kochający dom.
          </p>
        </div>

        <Row>
          <p style={{ marginBottom: 24 }}>
            Znaleźliśmy już domy dla {data.allStrapiCat.totalCount} kotów.
          </p>
          <SelectWrapper>
            <p>Sortuj: </p>
            <p style={{ width: 250 }}>
              <ReactSelect
                options={[
                  { value: "najnowsze", label: "od najnowszych" },
                  { value: "najstarsze", label: "od najstarszych" },
                  { value: "najmlodsze", label: "od najmłodszych" },
                  { value: "az", label: "A - Z" },
                  { value: "za", label: "Z - A" },
                ]}
                closeMenuOnSelect={true}
                hideSelectedOptions={false}
                components={{
                  Option,
                }}
                onChange={handleChange}
                value={selected}
                styles={{ menuPortal: base => ({ ...base, width: 500 }) }}
              />
            </p>
          </SelectWrapper>
        </Row>
        <List>
          {sorted
            .filter(({ node }) => node.zdjecia && node.zdjecia.length > 0)
            .map(({ node }) => (
              <CatCard
                img={node.zdjecia[0].url}
                name={node.imie.replace(" ", "")}
                descriptionData={{
                  wiek: getAge(node.dataUrodzenia),
                  płeć: node.plec,
                  "stosunek do kotów": node.stosunekDoKotow,
                  kastracja: node.kastracja,
                  "specjalne potrzeby": node.specjalnePotrzeby,
                }}
                href={removePolishChars(
                  node.imie.toLowerCase().replace(" ", "")
                )}
              />
            ))}
        </List>
      </Container>
    </Layout>
  )
}

export default WSwoichDomkachPage

export const query = graphql`
  query adoptowane {
    allStrapiCat(filter: { statusKota: { in: ["adoptowany"] } }) {
      totalCount
      edges {
        node {
          id
          imie
          kastracja
          opis {
            data {
              opis
            }
          }
          plec
          stosunekDoKotow
          statusKota
          dataUrodzenia
          specjalnePotrzeby
          createdAt
          zdjecia {
            url
          }
        }
      }
    }
  }
`
