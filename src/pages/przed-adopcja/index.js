import { Link } from "gatsby"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React from "react"
import styled from "styled-components"
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Image1 from "../../images/przedadopcja-image-1.png"
import Image2 from "../../images/przedadopcja-image-2.png"

const List = styled.ul`
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 10px;
  align-items: center;
  width: 100%;
  margin: 40px 0 24px 0;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    margin: 60px 0 24px 0;
    flex-direction: row;
  }
`

const ListElement = styled.li`
  width: 100%;
  overflow: hidden;
  max-width: 400px;
  box-sizing: border-box;
  a {
    display: flex;
    aspect-ratio: 1.5 / 1;
    color: ${({ theme }) => theme.colors.white};
    position: relative;
    justify-content: center;
    align-items: center;
    text-align: center;
    padding: 1rem;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      aspect-ratio: unset;
      height: 150px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      aspect-ratio: 1.5 / 1;
      height: auto;
    }
  }

  img {
    position: absolute;
    left: 0;
    top: 0;
    z-index: -1;
    width: 100%;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      top: 50%;
      transform: translateY(-50%);
    }

    &::after {
      content: "";
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, 0.5);
      z-index: -1;
    }
  }
`

const Container = styled.div`
  padding: 0 24px;
  position: relative;

  h2 {
    position: relative;
    padding-right: 80px;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 0;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 50px 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const PrzedAdopcjaPage = ({ pageContext }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  return (
    <Layout>
      <Seo title="Przed adopcją" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <h2>Przed adopcją</h2>
        <List>
          <ListElement>
            <Link to="warunki-adopcji">
              <img src={Image1} alt="" />
              <h4>Warunki adopcji</h4>
            </Link>
          </ListElement>
          <ListElement>
            <Link to="przygotowanie">
              <img src={Image2} alt="" />
              <h4>Jak przygotować się do adopcji kota?</h4>
            </Link>
          </ListElement>
        </List>
      </Container>
    </Layout>
  )
}

export default PrzedAdopcjaPage
