import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React from "react"
import styled from "styled-components"
import Layout from "../../../components/layout"
import Seo from "../../../components/seo"
import Icon1 from "../../../images/przygotowanie-icon-1.svg"
import Icon2 from "../../../images/przygotowanie-icon-2.svg"
import Icon3 from "../../../images/przygotowanie-icon-3.svg"
import Icon4 from "../../../images/przygotowanie-icon-4.svg"

const Container = styled.div`
  padding: 0 24px;
  position: relative;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 0 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 110px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }

  @media (min-width: 1560px) {
    &:after {
      top: 35px;
    }
  }
`

const Title = styled.h4`
  color: ${({ theme }) => theme.colors.accent};
  margin-top: 1.5rem;
`

const List = styled.ul`
  list-style-type: none;
  margin: 30px 0;
  padding: 0;

  li {
    display: flex;
    flex-direction: column;
    gap: 20px;
    margin: 40px 0;
  }
`

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 15px;
`

const MainTitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
  gap: 20px;
  width: 100%;

  /* @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
    width: 50%;
  } */

  @media (min-width: 1200px) {
    width: 50%;
  }

  @media (min-width: 1560px) {
    width: 100%;
  }

  h2 {
    position: relative;
    padding-right: 80px;
    width: 100%;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
      width: fit-content;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }
`

const Icon = styled.img``

const PrzygotowaniePage = ({ pageContext }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  return (
    <Layout>
      <Seo title="Jak przygotować się do adopcji?" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <MainTitleWrapper>
          <h2>Jak przygotować się do adopcji kota?</h2>
        </MainTitleWrapper>
        <div style={{ padding: "20px 0" }}>
          <p style={{ display: "contents" }}>
            W tym miejscu dzielimi się w wami przydatnymi artukułami, które
            poruszają najważniejsze tematy z zakresu przygotowania się do
            adopcji kota.
          </p>
          <List>
            <li>
              <TitleWrapper>
                <Icon src={Icon1} alt="" />
                <Title>Prawidłowe żywienie</Title>
              </TitleWrapper>
              <a href="https://blog.kocibehawioryzm.pl/2022/02/16/dlaczego-sucha-karma-to-zlo/">
                https://blog.kocibehawioryzm.pl/2022/02/16/dlaczego-sucha-karma-to-zlo/
              </a>
              <a href="https://www.przewodnikpokarmach.pl/koty/biblio/kot-bezwzgledny-miesozerca">
                https://www.przewodnikpokarmach.pl/koty/biblio/kot-bezwzgledny-miesozerca
              </a>
              <a href="http://kotuszkowo.pl/ranking-karm-dla-kotow-styczen-2022-r/">
                http://kotuszkowo.pl/ranking-karm-dla-kotow-styczen-2022-r/{" "}
              </a>
            </li>
            <li>
              <TitleWrapper>
                <Icon src={Icon2} alt="" />
                <Title>Gdy kot załatwia się poza kuwetą</Title>
              </TitleWrapper>
              <a href="https://blog.kocibehawioryzm.pl/2017/12/16/urynacja-poza-kuweta-a-badania/">
                https://blog.kocibehawioryzm.pl/2017/12/16/urynacja-poza-kuweta-a-badania/
              </a>
              <a href="https://blog.kocibehawioryzm.pl/2019/07/29/kocieta-a-problemy-kuwetowe/">
                https://blog.kocibehawioryzm.pl/2019/07/29/kocieta-a-problemy-kuwetowe/
              </a>
            </li>
            <li>
              <TitleWrapper>
                <Icon src={Icon3} alt="" />
                <Title>Wizyty weterynaryjne</Title>
              </TitleWrapper>
              <a href="http://kociweterynarz.pl/kontrola-zdrowia-kota/">
                http://kociweterynarz.pl/kontrola-zdrowia-kota/
              </a>
              <a href="http://kociweterynarz.pl/kot-u-weta/">
                http://kociweterynarz.pl/kot-u-weta/
              </a>
            </li>
            <li>
              <TitleWrapper>
                <Icon src={Icon4} alt="" />
                <Title>Koci behawioryzm</Title>
              </TitleWrapper>
              <a href="https://blog.kocibehawioryzm.pl/2022/04/30/czy-twoj-kot-potrzebuje-towarzystwa/">
                https://blog.kocibehawioryzm.pl/2022/04/30/czy-twoj-kot-potrzebuje-towarzystwa/
              </a>
              <a href="https://blog.kocibehawioryzm.pl/2020/05/01/kocie-poczucie-bliskosci/">
                https://blog.kocibehawioryzm.pl/2020/05/01/kocie-poczucie-bliskosci/
              </a>
              <a href="https://blog.kocibehawioryzm.pl/2020/02/25/agresja-u-kociaka/">
                https://blog.kocibehawioryzm.pl/2020/02/25/agresja-u-kociaka/
              </a>
            </li>
          </List>
        </div>
      </Container>
    </Layout>
  )
}

export default PrzygotowaniePage
