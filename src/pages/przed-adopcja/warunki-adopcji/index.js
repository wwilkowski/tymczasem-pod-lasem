import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React from "react"
import styled from "styled-components"
import Layout from "../../../components/layout"
import Seo from "../../../components/seo"
import Icon1 from "../../../images/warunki-adopcji-icon-1.svg"
import Icon2 from "../../../images/warunki-adopcji-icon-2.svg"
import Icon3 from "../../../images/warunki-adopcji-icon-3.svg"
import Icon4 from "../../../images/warunki-adopcji-icon-4.svg"
import { GlobalLink } from "../../../shared/styles"

export const Container = styled.div`
  padding: 0 24px;
  position: relative;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 0 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

export const Grid = styled.div`
  list-style-type: none;
  display: grid;
  margin: 1rem auto 3rem auto;
  grid-template-columns: 100%;
  position: relative;
  width: 100%;

  img {
    width: 60px;

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      width: unset;
    }
  }

  li:not(:last-child) {
    border-bottom: 2px solid ${({ theme }) => theme.colors.accent};
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    grid-template-columns: repeat(2, 1fr);
    width: fit-content;

    li:not(:last-child) {
      border-bottom: 0px solid ${({ theme }) => theme.colors.accent};
    }
    &::before {
      content: "";
      position: absolute;
      left: 50%;
      top: 0;
      width: 2px;
      height: 100%;
      transform: translateX(-50%);
      background-color: ${({ theme }) => theme.colors.accent};
    }

    &::after {
      content: "";
      position: absolute;
      left: 0;
      top: 50%;
      height: 2px;
      width: 100%;
      transform: translateY(-50%);
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }

  @media (min-width: 1480px) {
    grid-template-columns: repeat(2, 700px);
  }

  li {
    place-self: center;
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 20px;
    padding: 40px 0;

    @media (min-width: 1024px) {
      padding: 100px 0;
      margin: 0 50px;
    }
  }
`

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
  gap: 20px;
  width: 100%;

  h2 {
    position: relative;
    padding-right: 80px;
    width: 100%;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
      width: fit-content;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }
`

const WarunkiAdopcjiPage = ({ pageContext }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  return (
    <Layout>
      <Seo title="Warunki adopcji" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <TitleWrapper>
          <h2>Warunki adopcji</h2>
        </TitleWrapper>
        <GlobalLink
          href="https://forms.gle/nPRwb9kPye5zADuQA"
          target="_blank"
          style={{ marginTop: 40 }}
        >
          Wypełnij ankietę przedadopcyjną
        </GlobalLink>
        <Grid>
          <li>
            <img src={Icon1} alt="" />
            <h4>Szukamy domów niewychodzących</h4>
            <p>
              Podyktowane jest to troską o zdrowie i życie naszych
              podopiecznych, którye na zewnątrz spotkać może wiele
              niebezpieczeństw oraz sytuacji, które mogą skończyć się dla nich
              tragedią.
            </p>
          </li>
          <li>
            <img src={Icon2} alt="" />
            <h4>Zabezpieczenie balkonów i okien </h4>
            <p>
              Opiekunów w Nowym Domu prosimy o zabezpieczenie w szczególności
              balkonów. Osiatkowane okna oraz balkon zwiększają bezpieczeństwo
              kota.
            </p>
          </li>
          <li>
            <img src={Icon3} alt="" />
            <h4>Wizyta przedadopcyjna</h4>
            <p>
              Przed wydaniem kotka do nowego domu prosimy nowych właścicieli o
              możliwość przeprowadzenia wizyty przedadopcyjnej - szczególnie
              jeśli jest to dalsza adopcja i nie możemy dowieźć kotka osobiście.
              Wizyty przedadopcyjne są też formą pomocy dla nowych właścicieli,
              doradzimy, odpowiemy na ewentualne pytania, opowiemy więcej o
              danym kotku
            </p>
          </li>
          <li>
            <img src={Icon4} alt="" />
            <h4>Umowa adopcyjna</h4>
            <p>
              W dniu adopcji podpisujemy umowę adopcyjną. Z umową można
              zaznajomić się wcześniej, pobierając ją z naszej strony
              internetowej.
            </p>
            <GlobalLink href="/umowa-adopcyjna.pdf" target="blank">
              Pobierz umowę adopcyjną
            </GlobalLink>
          </li>
        </Grid>
      </Container>
    </Layout>
  )
}

export default WarunkiAdopcjiPage
