import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import * as React from "react"
import styled from "styled-components"
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import Image1Desktop from "../../images/zostan-tymczasem-image-1-desktop.png"
import Image1Mobile from "../../images/zostan-tymczasem-image-1-mobile.png"
import Image1Tablet from "../../images/zostan-tymczasem-image-1-tablet.png"
import Image2Desktop from "../../images/zostan-tymczasem-image-2-desktop.png"
import Image2Mobile from "../../images/zostan-tymczasem-image-2-mobile.png"
import Image2Tablet from "../../images/zostan-tymczasem-image-2-tablet.png"
import Image3Desktop from "../../images/zostan-tymczasem-image-3-desktop.png"
import Image3Mobile from "../../images/zostan-tymczasem-image-3-mobile.png"
import Image3Tablet from "../../images/zostan-tymczasem-image-3-tablet.png"
import Action1 from "../../images/zostantymczasem-actions-1.svg"
import Action2 from "../../images/zostantymczasem-actions-2.svg"
import Action3 from "../../images/zostantymczasem-actions-3.svg"
import { Flex } from "../../shared/styles"

const Container = styled.div`
  padding: 0 24px;
  position: relative;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 0 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const Content = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    gap: 27px;
    padding: 36px;
    padding-bottom: 0;

    a {
      margin-top: 20px;
    }
  }
`

const FirstSection = styled(Flex)`
  margin-bottom: 60px;
  gap: 20px;

  img {
    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      object-position: center;
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    gap: 14px;
    margin: 0 auto 150px auto;
  }

  picture {
    position: relative;

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      &::after {
        content: "";
        position: absolute;
        left: -30px;
        bottom: -30px;
        height: 317px;
        width: 96px;
        background-color: ${({ theme }) => theme.colors.accent};
        z-index: -1;
      }
    }
  }
`

const SecondSection = styled(Flex)`
  gap: 20px;
  margin: 0 auto 60px auto;

  img {
    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      object-position: center;
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    gap: 14px;
    margin: 0 auto 170px auto;
  }

  picture {
    position: relative;

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      &::after {
        content: "";
        position: absolute;
        right: -30px;
        bottom: -30px;
        height: 317px;
        width: 96px;
        background-color: ${({ theme }) => theme.colors.accent};
        z-index: -1;
      }
    }
  }
`

const ActionsList = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 60px;
  margin: 2rem auto 0 auto;
  text-align: center;

  @media (min-width: 480px) {
    flex-direction: row;
    justify-content: center;
    width: 100%;
  }
`

const ActionsElement = styled.li`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 10px;
  padding: 10px;

  p {
    max-width: 200px;
  }

  &:nth-child(2) {
    position: relative;

    &::before {
      content: "";
      height: 24px;
      width: 2px;
      background-color: ${({ theme }) => theme.colors.accent};
      position: absolute;
      bottom: 115%;
      left: 50%;
      transform: translateX(-50%);
    }

    &::after {
      content: "";
      height: 24px;
      width: 2px;
      background-color: ${({ theme }) => theme.colors.accent};
      position: absolute;
      top: 115%;
      left: 50%;
      transform: translateX(-50%);
    }

    @media (min-width: 480px) {
      &::before {
        content: "";
        width: 64px;
        height: 3px;
        background-color: ${({ theme }) => theme.colors.accent};
        position: absolute;
        left: -64px;
        top: 50%;
        transform: translate(0%, 50%);
      }

      &::after {
        content: "";
        width: 64px;
        height: 3px;
        background-color: ${({ theme }) => theme.colors.accent};
        position: absolute;
        left: 100%;
        top: 50%;
        transform: translate(0%, 50%);
      }
    }
  }

  img {
    width: 40px;
    height: 40px;

    @media (min-width: 480px) {
      width: 72px;
      height: 72px;
    }
  }
`

export const Wrapper = styled.div`
  padding: 24px 0;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 70px 0 48px 0;
  }
`

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
  gap: 20px;
  width: 100%;

  h2 {
    position: relative;
    padding-right: 80px;
    width: 100%;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
      width: fit-content;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }
`

const IndexPage = ({ pageContext }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  return (
    <Layout>
      <Seo title="Zostań Tymczasem" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <TitleWrapper>
          <h2>Zostań Tymczasem</h2>
        </TitleWrapper>
        <Wrapper>
          <FirstSection alignItems="flex-end">
            <picture>
              <source
                style={{ width: "100%" }}
                media="(max-width: 480px)"
                srcset={Image1Mobile}
                alt=""
              />
              <source
                style={{ width: "100%" }}
                media="(max-width: 1024px)"
                srcset={Image1Tablet}
                alt=""
              />
              <img style={{ width: "100%" }} src={Image1Desktop} alt-="" />
            </picture>
            <Content>
              <h2>Czym jest dom tymczasowy?</h2>
              <p>
                Dom tymczasowy jest miejscem, gdzie kot może zatrzymać się
                przejściowo - zanim zostanie adoptowany. W domach tymczasowych
                przyjęte przez nas koty poznają ciepło oraz opiekę człowieka, a
                w tym czasie my szukamy dla niego domu stałego. Domki tymczasowe
                są też miejscem, gdzie najlepiej poznajemy charakter kota,
                dzięki czemu możemy usprawnić proces adopcji.
              </p>
            </Content>
          </FirstSection>
          <SecondSection reverse alignItems="flex-end">
            <picture>
              <source
                style={{ width: "100%" }}
                media="(max-width: 480px)"
                srcset={Image2Mobile}
                alt=""
              />
              <source
                style={{ width: "100%" }}
                media="(max-width: 1024px)"
                srcset={Image2Tablet}
                alt=""
              />
              <img style={{ width: "100%" }} src={Image2Desktop} alt-="" />
            </picture>
            <Content>
              <h2>Jakie są zadania DT?</h2>
              <p>
                Najważniejszym zadaniem opiekunów tymczasowych kota jest opieka
                nad nim, czyli dbanie o jego dobro oraz stan zdrowia.
              </p>
              <p>
                Tymczasem pod lasem służy zawsze radami z zakresu żywienia kota,
                opieki weterynatryjnej oraz zagadnień behawiorystycznych.
              </p>
              <p>
                Opieka nad kotem może trwać czasami kilka dni, a czasami wiele
                miesięcy - zależy to od tego, jak potoczy się jego proces
                adopcyjny. Decydując się na założenie DT zawsze należy mieć to
                na uwadze.
              </p>
              <p>
                Bardzo cieszymy się również z fotorelacji z życia naszych
                podopiecznych!
              </p>
            </Content>
          </SecondSection>
          <FirstSection alignItems="flex-end">
            <picture>
              <source
                style={{ width: "100%" }}
                media="(max-width: 480px)"
                srcset={Image3Mobile}
                alt=""
              />
              <source
                style={{ width: "100%" }}
                media="(max-width: 1024px)"
                srcset={Image3Tablet}
                alt=""
              />
              <img style={{ width: "100%" }} src={Image3Desktop} alt-="" />
            </picture>
            <Content>
              <h2>Jak zostać Tymczasem?</h2>
              <p>
                Zawsze prosimy o to, by decyzja dotycząca założenia domu
                tymczasowego była dokładnie przemyślana!
              </p>
              <ActionsList>
                <ActionsElement>
                  <img src={Action1} alt="" />
                  <p>Skontaktuj się z nami</p>
                </ActionsElement>
                <ActionsElement>
                  <img src={Action2} alt="" />
                  <p>Poznajmy się lepiej i przygotujmy</p>
                </ActionsElement>
                <ActionsElement>
                  <img src={Action3} alt="" />
                  <p>Zaopiekuj się kotem</p>
                </ActionsElement>
              </ActionsList>
            </Content>
          </FirstSection>
        </Wrapper>
      </Container>
    </Layout>
  )
}

export default IndexPage
