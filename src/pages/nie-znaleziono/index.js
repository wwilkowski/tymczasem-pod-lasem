import React from "react"
import styled from "styled-components"
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import NotFoundImage from "../../images/notfound-image.png"

const Container = styled.div`
  padding: 0 24px;
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 4rem 0;
  gap: 20px;
  text-align: center;
`

const Image = styled.img`
  max-width: 200px;
`

const NieZnalezionoPage = () => (
  <Layout>
    <Seo title="Nie znaleziono" />
    <Container>
      <p>Wyniki wyszukiwania dla "Mruczek"</p>
      <Wrapper>
        <Image src={NotFoundImage} alt="" />
        <span>Nie odnaleźliśmy dopasowań dla Twojego zapytania :(</span>
      </Wrapper>
    </Container>
  </Layout>
)

export default NieZnalezionoPage
