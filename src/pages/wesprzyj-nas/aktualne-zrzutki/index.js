import { graphql, Link } from "gatsby"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React from "react"
import styled from "styled-components"
import Layout from "../../../components/layout"
import Seo from "../../../components/seo"

const Container = styled.div`
  padding: 0 24px;
  position: relative;

  h2 {
    position: relative;
    padding-right: 80px;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 50px 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const List = styled.ul`
  margin: 50px auto;
  display: flex;
  flex-direction: column;
  gap: 20px;
`

const AktualneZrzutkiPage = ({ pageContext, data }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  return (
    <Layout>
      <Seo title="Kup karmę" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <h2>Aktualne zrzutki</h2>
        <List>
          {data.allStrapiZbiorka.edges.map(({ node }) => (
            <h5>
              <Link href={node.link} target="_blank">
                {node.nazwa}{" "}
              </Link>
            </h5>
          ))}
        </List>
      </Container>
    </Layout>
  )
}

export default AktualneZrzutkiPage

export const query = graphql`
  {
    allStrapiZbiorka {
      edges {
        node {
          nazwa
          link
        }
      }
    }
  }
`
