import { Link } from "gatsby"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React from "react"
import styled from "styled-components"
import Layout from "../../../components/layout"
import Seo from "../../../components/seo"
import Karma2 from "../../../images/karmy/image10.png"
import Karma3 from "../../../images/karmy/image11.png"
import Karma4 from "../../../images/karmy/image12.png"
import Karma5 from "../../../images/karmy/image13.png"
import Karma6 from "../../../images/karmy/image14.png"
import Karma7 from "../../../images/karmy/image15.png"
import Karma8 from "../../../images/karmy/image17.png"
import Karma1 from "../../../images/karmy/image9.png"

const Container = styled.div`
  padding: 0 24px 40px;
  position: relative;

  h2 {
    position: relative;
    padding-right: 80px;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 50px 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const List = styled.ul`
  margin: 100px auto 60px;
  display: grid;
  grid-template-columns: 1fr;
  justify-items: center;
  gap: 60px;

  @media (min-width: 460px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (min-width: 700px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media (min-width: 900px) {
    grid-template-columns: repeat(4, 1fr);
  }

  max-width: 1200px;

  h5 {
    text-align: center;
    font-weight: 600;
  }

  li {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 20px;
  }
`

const Info = styled.span`
  color: ${({ theme }) => theme.colors.accent};
`

const KupKarmePage = ({ pageContext }) => {
  const {
    breadcrumb: { crumbs },
  } = pageContext

  return (
    <Layout>
      <Seo title="Kup karmę" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <h2>Karmy, których potrzebujemy</h2>
        <List>
          <li>
            <img src={Karma1} alt="" />
            <h5>Smilla</h5>
          </li>
          <li>
            <img src={Karma2} alt="" />
            <h5>Animonda Carny</h5>
          </li>
          <li>
            <img src={Karma3} alt="" />
            <h5>Feringa</h5>
          </li>
          <li>
            <img src={Karma4} alt="" />
            <h5>Dolina Noteci</h5>
          </li>
          <li>
            <img src={Karma5} alt="" />
            <h5>Mac's</h5>
          </li>
          <li>
            <img src={Karma6} alt="" />
            <h5>Butcher's</h5>
          </li>
          <li>
            <img src={Karma7} alt="" />
            <h5>Kitty Premium</h5>
          </li>
          <li>
            <img src={Karma8} alt="" />
            <h5>Maxi Natural</h5>
          </li>
        </List>
        <h5 style={{ lineHeight: 2 }}>
          Chcesz wysłać paczkę z karmą dla naszych kotków?
          <br />
          Napisz do nas na
          <Info> kontakt@tymczasempodlasem.pl</Info> lub na{" "}
          <Link
            href="https://m.me/tymczasempodlasem"
            target="_blank"
            style={{ color: "#BB2205" }}
          >
            Messengerze
          </Link>
          !
        </h5>
      </Container>
    </Layout>
  )
}

export default KupKarmePage
