import { Link } from "gatsby"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import React, { useState } from "react"
import styled from "styled-components"
import Layout from "../../components/layout"
import Seo from "../../components/seo"
import ImageDesktop from "../../images/kontakt-image-desktop.png"
import ImageMobile from "../../images/kontakt-image-mobile.png"
import ImageTablet from "../../images/kontakt-image-tablet.png"

const Section = styled.section`
  padding: 0 24px;
`

const Title = styled.h3`
  color: ${({ theme }) => theme.colors.accent};
  margin-bottom: 24px;

  @media (min-width: 1024px) {
    margin-bottom: 0;
  }
`

const Input = styled.input`
  border: 1px solid #2e2e2e;
  display: block;
  width: 100%;
  padding: 16px;
`

const Textarea = styled.textarea`
  border: 1px solid #2e2e2e;
  display: block;
  width: 100%;
  padding: 16px;
  height: 300px;
`

const Label = styled.label`
  font-size: #000;
  margin: 24px 0 8px 0;
  display: block;
`

const Button = styled.button`
  background-color: ${({ theme }) => theme.colors.accent};
  text-transform: uppercase;
  color: ${({ theme }) => theme.colors.secondaryText};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 13px 39px 16px;
  gap: 10px;
  box-shadow: 2px 2px 5px rgba(46, 46, 46, 0.5);
  border-radius: 40px;
  width: fit-content;
  font-size: 14px;
  width: 100%;
  white-space: nowrap;
  margin: 24px 0;

  @media (min-width: 480px) {
    font-size: 18px;
  }

  @media (min-width: 1024px) {
    font-size: 20px;
  }
`

const Form = styled.form`
  margin-top: 24px;

  label &:nth-child(0) {
    background-color: red;
  }
`

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  gap: 64px;
  position: relative;
  margin-top: 30px;

  @media (min-width: 1024px) {
    padding-top: 30px;

    grid-template-columns: 1fr 1fr;
  }

  &::before {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 3px;
    background-color: ${({ theme }) => theme.colors.accent};
  }
`

const Text = styled.p`
  width: 100%;
  display: inline;
`

const Image = styled.img`
  display: none;

  @media (min-width: 1024px) {
    display: block;
    width: 100%;
    max-width: 700px;
    margin: 2rem auto 0 auto;
  }
`

const Container = styled.div`
  padding: 0 24px;
  position: relative;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 0 100px 100px 100px;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: 35px;
      right: 0;
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const Description = styled.div`
  padding: 24px 0 12px 0;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 24px 0;
  }
`

const TitleWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-end;
  gap: 20px;
  width: 100%;

  h2 {
    position: relative;
    padding-right: 80px;
    width: 100%;

    @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
      padding-right: 240px;
    }

    @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
      padding-right: 0;
      width: fit-content;
    }

    &::after {
      content: "";
      position: absolute;
      width: 80px;
      height: 48px;
      bottom: 9px;
      right: -24px;
      background-color: ${({ theme }) => theme.colors.accent};

      @media (min-width: ${({ theme }) => theme.breakpoints.tablet}) {
        width: 240px;
        height: 64px;
      }

      @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
        display: none;
      }
    }
  }
`

const JeszczeNiegotowePage = ({ pageContext }) => {
  const [firstName, setFirstName] = useState("")
  const [email, setEmail] = useState("")
  const [message, setMessage] = useState("")

  const {
    breadcrumb: { crumbs },
  } = pageContext

  return (
    <Layout>
      <Seo title="Kontakt" />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <TitleWrapper>
          <h2>Kontakt</h2>
        </TitleWrapper>
        <Description>
          <Text>
            W sprawie adopcji kotów lub w przypadku jakichkolwiek pytań
            dotyczących procesu adopcyjnego lub naszych podopiecznych -
            zapraszamy do kontaktu przez{" "}
            <Link
              href="https://m.me/tymczasempodlasem"
              target="_blank"
              style={{ color: "#BB2205" }}
            >
              Messenger
            </Link>{" "}
            oraz formularz kontaktowy.
          </Text>
        </Description>
        <Wrapper>
          <Form action="https://formspree.io/f/xgebwzbv" method="POST">
            <Title>Napisz do nas!</Title>
            <picture>
              <source media="(max-width: 480px)" srcset={ImageMobile} alt="" />
              <source media="(max-width: 1024px)" srcset={ImageTablet} alt="" />
              <img style={{ width: "100%" }} src="" alt-="" />
            </picture>
            <Label>Twoje imię</Label>
            <Input
              type="text"
              name="imie"
              value={firstName}
              onChange={e => setFirstName(e.target.value)}
              placeholder="np. Anna"
            />
            <Label>Adres e-mail</Label>
            <Input
              type="text"
              name="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              placeholder="twój@email.com"
            />
            <Label>Treść wiadomości</Label>
            <Textarea
              value={message}
              onChange={e => setMessage(e.target.value)}
              placeholder="W czym możemy Ci pomóc?"
              name="tresc"
            />
            <Button onClick={() => {}}>Wyślij</Button>
          </Form>

          <Image src={ImageDesktop} alt-="" />
        </Wrapper>
      </Container>
    </Layout>
  )
}

export default JeszczeNiegotowePage
