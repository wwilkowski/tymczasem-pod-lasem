import { Link } from "gatsby"
import styled from "styled-components"

export const Space = styled.div`
  padding-left: 24px;
  padding-right: 24px;

  @media (min-width: 1024px) {
    padding: 0;
  }
`

export const GlobalLink = styled(Link)`
  background-color: ${({ theme }) => theme.colors.accent};
  text-transform: uppercase;
  color: ${({ theme }) => theme.colors.secondaryText};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 13px 39px 16px;
  gap: 10px;
  box-shadow: 2px 2px 5px rgba(46, 46, 46, 0.5);
  border-radius: 40px;
  width: fit-content;
  font-size: 14px;
  width: 100%;
  white-space: nowrap;
  font-weight: 500;

  @media (min-width: 480px) {
    font-size: 18px;
  }

  @media (min-width: 1024px) {
    width: fit-content;
    font-size: 20px;
  }
`

export const Flex = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  flex-wrap: ${({ wrap }) => (wrap ? "wrap" : "nowrap")};
  gap: ${({ gap }) => gap || 0};

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    align-items: ${({ alignItems }) => alignItems || "flex-start"};
    flex-direction: ${({ reverse }) => (reverse ? "row-reverse" : "row")};
    justify-content: center;
  }
`
