export const getAge = date => {
  if (!date) return "brak danych"
  const currentDate = new Date()
  const birthDate = new Date(date)

  const ageInMilliseconds = currentDate - birthDate
  const ageInMonths = Math.floor(ageInMilliseconds / (1000 * 60 * 60 * 24 * 30))

  const ageInYears = Math.floor(ageInMonths / 12)

  let ageString = ""
  if (ageInYears === 1) {
    ageString = `${ageInYears} rok`
  } else if (ageInYears > 1 && ageInYears <= 4) {
    ageString = `${ageInYears} lata`
  } else if (ageInYears > 4) {
    ageString = `${ageInYears} lat`
  }

  if (ageInMonths % 12 === 1) {
    ageString += ` ${ageInMonths % 12} miesiąc`
  } else if (ageInMonths % 12 > 1 && ageInMonths % 12 <= 4) {
    ageString += ` ${ageInMonths % 12} miesiące`
  } else if (ageInMonths % 12 > 4) {
    ageString += ` ${ageInMonths % 12} miesięcy`
  }

  return ageString
}

export const removePolishChars = str => {
  const polishChars = "ąćęłńóśźż"
  const normalChars = "acelnoszz"
  let newString = ""

  for (let i = 0; i < str.length; i++) {
    let char = str[i]
    let index = polishChars.indexOf(char)
    if (index >= 0) {
      char = normalChars[index]
    }
    newString += char
  }

  return newString
}
