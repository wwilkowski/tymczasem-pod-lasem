import React from "react"
import ReactDOM from "react-dom"
import styled from "styled-components"

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.7);
  position: fixed;
  left: 0;
  top: 0;
  z-index: 1000;
  display: flex;
`

const Modal = ({ showModal, setShowModal, image }) => {
  const portalRoot =
    typeof document !== `undefined` ? document.getElementById("portal") : null

  return (
    showModal &&
    ReactDOM.createPortal(
      <Container
        onClick={() => setShowModal(false)}
        onScroll={e => e.stopPropagation()}
      >
        <div className="modal-content">
          <button onClick={() => setShowModal(false)}>Close</button>

          <img src={`https://admin.tymczasempodlasem.pl${image}`} />
        </div>
      </Container>,
      portalRoot
    )
  )
}

export default Modal
