export const theme = {
  colors: {
    white: "#FFFFFF",
    background: "#FDFDFD",
    primaryText: "#2E2E2E",
    secondaryText: "#FDFDFD",
    accent: "#BB2205",
    secondaryAccent: "#FFB692",
  },
  fontSizes: {
    small: "1em",
    medium: "2em",
    large: "3em",
  },
  breakpoints: {
    tablet: "480px",
    desktop: "1200px",
  },
  fontWeight: {
    medium: 600,
  },
}
