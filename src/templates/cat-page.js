import { Link } from "gatsby"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import { GatsbyImage } from "gatsby-plugin-image"
import * as React from "react"

import styled from "styled-components"
import Carousel from "../components/Carousel"
import Layout from "../components/layout"
import Seo from "../components/seo"
import AlertIcon from "../images/icons/alert.svg"
import { getAge } from "../shared/helpers"
import { GlobalLink } from "../shared/styles"

export const Container = styled.div`
  padding: 0 24px;
  position: relative;
  max-width: 1920px;
  margin: 0 auto;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 50px 100px 0 100px;
    position: relative;
  }
`

const Collections = styled.div`
  color: ${({ theme }) => theme.colors.accent};
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
  max-width: 500px;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    position: absolute;
    top: 9rem;
    right: 6rem;
    text-align: right;
    align-items: flex-end;
  }

  h5 {
    font-weight: 600;
  }

  ul {
    list-style-type: disc;
    margin-top: 0.5rem;
    padding-left: 0.4rem;
  }

  li {
    padding: 0.1rem 0;
  }

  a {
    color: ${({ theme }) => theme.colors.accent};

    text-decoration: none;
  }
`

const AlertIconImg = styled.img`
  width: 40px;
  height: 40px;
`

const Important = styled.span`
  font-weight: 600;
  display: inline-block;
  margin: 10px 0;
`

const Section = styled.section`
  display: flex;
  flex-direction: column;
  gap: 20px;
  padding: 2rem 0 3rem 0;
  width: 100%;

  @media (min-width: 800px) {
    flex-direction: row;
    align-items: flex-end;
  }

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    padding: 2rem 0 90px 0;
  }
`

const CatImage = styled.img`
  width: 400px;
`

const Description = styled.p`
  display: contents;
  width: 100%;
  margin: 40px 0;
  line-height: 24px;
  white-space: pre-wrap;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    line-height: 36px;
  }
`

const Contact = styled.div`
  margin: 4rem 0;
  position: relative;

  @media (min-width: 1250px) {
    &::after {
      content: "";
      position: absolute;
      width: 496px;
      height: 96px;
      top: -35px;
      right: 0;
      transform: translateX(50%);
      background-color: ${({ theme }) => theme.colors.accent};
    }
  }
`

const StyledGatsbyImage = styled(GatsbyImage)`
  width: 100%;
  aspect-ratio: 1 / 1.14;
  max-width: 400px;
`

const CatDescription = styled.div`
  margin: 20px 0 30px 0;

  @media (min-width: ${({ theme }) => theme.breakpoints.desktop}) {
    margin: 20px 0 90px 0;
  }
`

const CatPage = ({ pageContext }) => {
  const {
    imie,
    dataUrodzenia,
    zdjecia,
    plec,
    stosunekDoKotow,
    opis,
    specjalnePotrzeby,
    kastracja,
    zbiorki,
  } = pageContext.node

  const {
    breadcrumb: { crumbs },
  } = pageContext

  const isAdoption = crumbs.some(crumb =>
    crumb.pathname.includes("w-swoich-domkach")
  )

  return (
    <Layout>
      <Seo title={imie} imageurl={imie.toLowerCase()} />
      <Container>
        <Breadcrumb crumbs={crumbs} crumbSeparator=" > " />
        <Section>
          <img
            className="catpageimage"
            src={`https://admin.tymczasempodlasem.pl${zdjecia[0].url}`}
          />

          <div>
            <h3>
              {imie} {isAdoption && "(adoptowany/a)"}
            </h3>
            {dataUrodzenia ? (
              <p>
                <Important>wiek: </Important> {getAge(dataUrodzenia)}
              </p>
            ) : null}
            {plec ? (
              <p>
                <Important>płeć: </Important> {plec}
              </p>
            ) : null}
            {stosunekDoKotow ? (
              <p>
                <Important>stosunek do kotów: </Important> {stosunekDoKotow}
              </p>
            ) : null}
            {kastracja ? (
              <p>
                <Important>kastracja: </Important> {kastracja}
              </p>
            ) : null}
            {specjalnePotrzeby ? (
              <p>
                <Important>specjalne potrzeby: </Important> {specjalnePotrzeby}
              </p>
            ) : null}
          </div>
          <Collections>
            <div style={{ display: "flex", alignItems: "flex-end", gap: 10 }}>
              <AlertIconImg src={AlertIcon} alt="" />
              <h5>Aktualne zbiórki: </h5>
            </div>
            <ul>
              {zbiorki && zbiorki.length ? (
                zbiorki.map(({ nazwa, link }) => (
                  <li>
                    <a href={link} target="_blank">
                      {nazwa}
                    </a>
                  </li>
                ))
              ) : (
                <li>
                  <a href="https://zrzutka.pl/shbh5b" target="_blank">
                    Wspomóż zakup karmy i żwiru dla tymczasków
                  </a>
                </li>
              )}
            </ul>
            <GlobalLink
              style={{ color: "#fff", margin: "1rem 0", fontSize: 14 }}
              href={
                zbiorki && zbiorki.length
                  ? zbiorki[0].link
                  : "https://zrzutka.pl/shbh5b"
              }
              target="_blank"
            >
              Wesprzyj mnie!
            </GlobalLink>
          </Collections>
        </Section>
        {opis && opis.data.opis.length > 0 && (
          <>
            <h3>Opis kota</h3>
            <CatDescription>
              <Description>{opis ? opis.data.opis : "brak"}</Description>
            </CatDescription>
          </>
        )}
        {zdjecia && zdjecia.length > 0 && (
          <Carousel
            images={zdjecia.filter(photo => photo).map(photo => photo.url)}
          />
        )}
        {!isAdoption && (
          <Contact>
            <h2>Skontaktuj się z nami</h2>
            <div style={{ margin: "20px 0 30px 0" }}>
              <Description>
                Jeżeli kotek skradł Twoje serce i chcesz go adoptować, wypełnij
                ankietę adopcyjną, a my skontaktujemy się z Tobą!
              </Description>
            </div>
            <GlobalLink
              href="https://forms.gle/nPRwb9kPye5zADuQA"
              target="_blank"
            >
              Wypełnij ankietę
            </GlobalLink>
            <div style={{ margin: "30px 0 20px 0" }}>
              <Description>
                Chcesz dowiedzieć się więcej na temat kota lub procesu
                adopcyjnego? Chętnie odpowiemy na wszystkie Twoje pytania!
              </Description>
            </div>
            <p>
              <Link
                to="/kontakt"
                style={{ color: "#BB2205", textDecoration: "underline" }}
              >
                Wypełnij formularz kontaktowy
              </Link>
            </p>
          </Contact>
        )}
      </Container>
    </Layout>
  )
}

export default CatPage
