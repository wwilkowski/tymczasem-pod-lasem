const fs = require("fs-extra")
const axios = require("axios")
const path = require("path")

function removePolishChars(str) {
  const polishChars = "ąćęłńóśźż"
  const normalChars = "acelnoszz"
  let newString = ""

  for (let i = 0; i < str.length; i++) {
    let char = str[i]
    let index = polishChars.indexOf(char)
    if (index >= 0) {
      char = normalChars[index]
    }
    newString += char
  }

  return newString
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const catPage = path.resolve(`./src/templates/cat-page.js`)

  return graphql(
    `
      {
        allStrapiCat {
          edges {
            node {
              id
              imie
              kastracja
              opis {
                data {
                  opis
                }
              }
              plec
              stosunekDoKotow
              statusKota
              dataUrodzenia
              specjalnePotrzeby
              zbiorki {
                nazwa
                link
              }
              zdjecia {
                url
              }
            }
          }
        }
        allStrapiZbiorka {
          nodes {
            id
            nazwa
            link
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }
    const nodes = result.data.allStrapiCat.edges
    const collections = result.data.allStrapiZbiorka
    nodes.forEach(async ({ node }) => {
      if (node.statusKota === "do adopcji") {
        createPage({
          path:
            "nasi-podopieczni/koty-do-adopcji/" +
            removePolishChars(node.imie.toLowerCase().replace(" ", "")),
          component: catPage,
          context: {
            node,
            collections,
            imie: node.imie.toLowerCase().replace(" ", ""),
          },
        })
      } else if (node.statusKota === "niegotowy") {
        createPage({
          path:
            "nasi-podopieczni/jeszcze-niegotowe/" +
            removePolishChars(node.imie.toLowerCase().replace(" ", "")),
          component: catPage,
          context: {
            node,
            collections,
            imie: node.imie.toLowerCase().replace(" ", ""),
          },
        })
      } else {
        createPage({
          path:
            "nasi-podopieczni/w-swoich-domkach/" +
            removePolishChars(node.imie.toLowerCase()),
          component: catPage,
          context: {
            node,
            collections,
            imie: node.imie.toLowerCase().replace(" ", ""),
          },
        })
      }
    })
  })
}
