module.exports = {
  siteMetadata: {
    title: `Tymczasem pod lasem`,
    description: `
    We współpracy z kilkoma domami tymczasowymi pomagamy kotom z obszarów wiejskich z rejonów Hajnówki i Torunia. Działamy prywatnie, w miarę możliwości. W okolicy Hajnówki szanse na adopcje są nikłe dlatego szukamy domów w całej Polsce.`,
    author: `@wwilkowski`,
    siteUrl: `https://tymczasempodlasem.pl`,
  },
  plugins: [
    `gatsby-plugin-image`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: [
          "G-KVPWBBDB8H", // Google Analytics / GA
        ],
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: false,
        },
      },
    },
    {
      resolve: `gatsby-plugin-portal`,
      options: {
        key: "portal",
        id: "portal",
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-plugin-breadcrumb",
      options: {
        useAutoGen: true,
        autoGenHomeLabel: `Strona główna`,
        crumbLabelUpdates: [
          {
            pathname: "/nasi-podopieczni",
            crumbLabel: "Nasi podopieczni",
          },
          {
            pathname: "/nasi-podopieczni/koty-do-adopcji",
            crumbLabel: "Koty do adopcji",
          },
          {
            pathname: "/nasi-podopieczni/jeszcze-niegotowe",
            crumbLabel: "Jeszcze niegotowe",
          },
          {
            pathname: "/nasi-podopieczni/w-swoich-domkach",
            crumbLabel: "W swoich domkach",
          },
          {
            pathname: "/przed-adopcja",
            crumbLabel: "Przed adopcją",
          },
          {
            pathname: "/przed-adopcja/warunki-adopcji",
            crumbLabel: "Warunki adopcji",
          },
          {
            pathname: "/przed-adopcja/przygotowanie",
            crumbLabel: "Jak przygotować się do adopcji kota?",
          },
          {
            pathname: "/zostan-tymczasem",
            crumbLabel: "Zostań tymczasem",
          },
          {
            pathname: "/wesprzyj-nas",
            crumbLabel: "Wesprzyj nas",
          },
          {
            pathname: "/kontakt",
            crumbLabel: "Kontakt",
          },
        ],
      },
    },
    `gatsby-transformer-sharp`,
    // {
    //   resolve: `gatsby-source-datocms`,
    //   options: {
    //     apiToken: `8f7f7287d7f43e61fc0ac5d872d1d4`,
    //     environment: `main`,
    //     previewMode: false,
    //     disableLiveReload: false,
    //     pageSize: 500,
    //   },
    // },
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-strapi`,
      options: {
        apiURL: "https://admin.tymczasempodlasem.pl",
        accessToken:
          "fb892e8e2ae37ec5dcdd12623bfc166ac1f0dcc840823d1bdf67c0bee2d89703189fe2c67db78e9bf414fe850cb722ee886615d2fe972e319932fcd059f0bf25a89c0319ec968974d57dfaabb26ae2c379edc7250362671e2dcaddcae4446a0da19828860b25645506e52dab175540af67c2bd3aeed1121953d4d24a37e2411e",
        collectionTypes: [
          "cat",
          "kadr-z-akcji",
          "collection",
          "zbiorka",
          "zbiorki",
        ],
        singleTypes: [],
        cache: false,
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: ["/nasi-podopieczni/*"],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        // This will impact how browsers show your PWA/website
        // https://css-tricks.com/meta-theme-color-and-trickery/
        // theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
